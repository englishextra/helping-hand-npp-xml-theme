HelpingHand Notepad++ XML Theme
===============================

Based on coda-textMate-theme <https://github.com/chriskempson/coda-textMate-theme>

Authors
-------

**Serguei Shimansky**

+ https://twitter.com/englishextra

+ https://bitbucket.org/englishextra

Copyright and License
---------------------

Copyright 2012 Serguei Shimansky
